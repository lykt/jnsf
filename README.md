# English keyboard layouts

Carpalx and SWORD optimized English keyboard layouts

## Classic layouts

Available at [http://mkweb.bcgsc.ca/carpalx/](http://mkweb.bcgsc.ca/carpalx/)

## Carpalx optimizations with preset home keys

 * [JNSF layout](https://lykt.xyz/jnsf/)
 * [JNRF layout](https://lykt.xyz/jnsf/retro/)
 * [FNRS layout](https://lykt.xyz/jnsf/5/)
 * [Orthocarpus layout](https://lykt.xyz/jnsf/ortho/)
 * [Eiþ layout](https://lykt.xyz/þorn/#eiþ)

## Carpalx optimized adaptive layouts

 * [Horn layout](https://lykt.xyz/horn/)
 * [Capricorn layout](https://lykt.xyz/horn/#capricorn)
 * [Orthon layout](https://lykt.xyz/rtsd/#orthon)
 * [Netto layout](https://lykt.xyz/rtsd/#netto)
 * [Ant layout](https://lykt.xyz/rtsd/#ant)
 * [Tin layout](https://lykt.xyz/rtsd/#tin)
 * [Seal layout](https://lykt.xyz/seal/)
 * [Real layout](https://lykt.xyz/seal/#real)
 * [Rina layout](https://lykt.xyz/nari/#rina)
 * [Nari layout](https://lykt.xyz/nari/)
 * [Uno layout](https://lykt.xyz/uno/)
 * [One layout](https://lykt.xyz/uno/#one)
 * [Unit layout](https://lykt.xyz/uno/#unit)
 * [Quant layout](https://lykt.xyz/uno/#quant)

## SWORD optimizations

 * [Rain layout](https://lykt.xyz/rain/)
 * [Snow layout](https://lykt.xyz/snow/)
 * [Aries layout](https://lykt.xyz/tandem/#aries)
 * [Unicorn layout](https://lykt.xyz/tandem/#unicorn)
 * [Orthostat layout](https://lykt.xyz/orthostat/)
 * [Norto layout](https://lykt.xyz/skl/norto/#en)
 * [U-turn layout](https://lykt.xyz/u-turn/)
 * [Saint layout](https://lykt.xyz/saint/)
 * [Þunder layout](https://lykt.xyz/þorn/#þunder)
 * [Tensor layout](https://lykt.xyz/þorn/#tensor)

## Carpalx optimizations with Þ key

 * [Þorn layout](https://lykt.xyz/þorn/#þorn)
 * [Goldþorn layout](https://lykt.xyz/þorn/#goldþorn)
 * [Yup layout](https://lykt.xyz/þorn/#yup)
 * [Eiþ layout](https://lykt.xyz/þorn/#eiþ)
